package com.example.student.ToDoPA;

import android.net.Uri;


public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}
