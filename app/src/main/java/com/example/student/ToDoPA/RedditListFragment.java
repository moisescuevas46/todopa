package com.example.student.ToDoPA;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.student.ToDoPA.R;

public class RedditListFragment extends Fragment {
    private MainActivity activity;



    private RecyclerView recyclerView;
//    private ActivityCallback activityCallback;
//    private MainActivity activity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        activityCallback = (ActivityCallback)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        activityCallback = null;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        RedditPost str = new RedditPost("assessment","10/30/15","11/2/15","App Academy");
        RedditPost str1 = new RedditPost("assessment","10/30/15","11/2/15","App Academy");
        RedditPost str2 = new RedditPost("assessment","10/30/15","11/2/15","App Academy");
        RedditPost str3 = new RedditPost("assessment","10/30/15","11/2/15","App Academy");

        activity.RedditPost.add(str);
        activity.RedditPost.add(str1);
        activity.RedditPost.add(str2);
        activity.RedditPost.add(str3);

        RedditPostAdapter adapter = new RedditPostAdapter();
        recyclerView.setAdapter(adapter);



        return view;
    }
}




